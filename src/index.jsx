import api from "@forge/api";
import ForgeUI, { render, Fragment, Macro, Text, Select, Option, useState, useConfig, ConfigForm, TextField} from "@forge/ui";

const getQuote = async (fromCurrency, toCurrency, amount) => {

  const res = await api
    .fetch(`https://api.exchangerate.host/convert?from=${fromCurrency}&to=${toCurrency}&amount=${amount}&places=2`);

  const data = await res.json();
  return data;
};

const getSymbols = async () => {
  const res = await api
    .fetch(`https://api.exchangerate.host/symbols`);

  const data = await res.json();
  return data.symbols;
}

const getLocale = async (accountId) => {

  const res = await api
    .asUser()
    .requestJira(`/rest/api/3/myself`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    });

    const data = await res.json();
    return data;
}

const App = () => {

  const config = useConfig();
  let result;
  if (isNaN(config.amount)) {
    result = "Error: Amount needs to be a number";
  } else {
    const [ currencyData ] = useState(async () => await getQuote(config.fromCurrency, config.toCurrency, config.amount)) 
    const [ localeData ] = useState(async () => await getLocale());
    const fixedLocale = localeData.locale.replace('_','-'); // WHY isn't it '-' to start with???
    const fromAmount = new Intl.NumberFormat(fixedLocale, { style: 'currency', currency: config.fromCurrency }).format(parseFloat(config.amount)); 
    const toAmount = new Intl.NumberFormat(fixedLocale, { style: 'currency', currency: config.toCurrency }).format(parseFloat(currencyData.result)); 
    result = `${fromAmount} = ${toAmount}`;
  };

  return (
    <Fragment>
      <Text>{result}</Text>
    </Fragment>
  );
};

// Function that defines the configuration UI
const Config = () => {

  const [ symbols ] = useState(async () => await getSymbols());
  let options = [];

  Object.keys(symbols).forEach(function(currency) {
    const label = `${symbols[currency].code} (${symbols[currency].symbol})`
    options.push(<Option label={label} value={symbols[currency].code} />);
  }); 

  return (
    <ConfigForm>
      <Select label="From currency: " name="fromCurrency">
        { options }
      </Select>
      <Select label="To currency: " name="toCurrency">
        { options }
      </Select>
      <TextField name="amount" label="Amount: " />
    </ConfigForm>
  );
};

export const run = render(
  <Macro
    app={<App />}
    config={<Config />}
    defaultConfig={{
      fromCurrency: "USD",
      toCurrency: "AUD",
      amount: "1"
    }}
  />
);  